
    /* let elemento;

    elemento = document.all;    
    elemento = document.all[10];
    elemento = document.URL;
    elemento = document.images;
    elemento = document.forms;
    
    
    
    elemento = document.images;

    let imagenes = document.images;
    let imagesArr = Array.from( imagenes );
    
    imagesArr.forEach( function ( imagen ) {
        console.log( imagen );
    });

    console.log( imagesArr ); */

    //getElementById

    /* let elemento;
        let encabezado;

        encabezado = document.getElementById('encabezado');
        encabezado.style.background = ' #333 ' ;
        encabezado.style.color = ' #fff';
        encabezado.style.padding = ' 30px';
        encabezado.innerText = " Un gran Curso";

        console.log( encabezado ); */



    // Query Selector

        //const puff = document.querySelector('.encabezado');
      /*  let enlace;
        enlace = document.querySelector('#principal a:first-child');
        enlace = document.querySelector('#principal a:last-child');
        enlace = document.querySelector('#principal a:nth-child(4)');
        //Aplicar css
        

        console.log( enlace );*/


    /* const links = document.getElementsByTagName('a');

    let enlaces = Array.from(links);

    enlaces.forEach(function (enlace) {
        console.log(enlace.innerText);
    })
    //console.log(enlaces); */

    /*const enlaces = document.querySelectorAll ('#principal a:nth-child(odd)');
    enlaces.forEach(function (impares) {
        impares.style.backgroundColor = 'red';
        impares.style.color = 'white';
    });
    console.log (enlaces);*/


    //Selectores

       // const navegacion = document.querySelector('#principal');
       // const barra = document.querySelector('.barra');
       // console.log( barra.children[0].textContent);
        
        // 1 = Elementos HTML   
        // 2 = Atributos
        // 3 = Text Node
        // 8 = comentarios
        // 9 = documentos 
        // 10 = doctype 


        /*const enlaces = document.querySelectorAll('.enlace');
        console.log(enlaces[0].parentElement);*/


        //Crear elementos desde JS

            //crear enlace
       /*         const enlace = document.createElement('a');

                //agregamos una clase
                enlace.className = 'enlace';
                //añadir Id
                enlace.id = 'nuevo-id';
                //  atributo href
                enlace.setAttribute('href','#');
                //Añadir texto
                enlace.textContent = 'Nuevo Enlace';


                //Agregarlo al html
                document.querySelector('#secundaria').appendChild(enlace);

                console.log(enlace); */


   
    //Modificar elementos (nodos)
    
        /*const nuevoEncabezado = document.createElement('h2');

        //agregar un id
        nuevoEncabezado.id = 'encabezado';
        //texto
        nuevoEncabezado.appendChild(document.createTextNode('Los mejores Cursos'));
        nuevoEncabezado.align = 'center';
        //elemento anterior (sera remplazado)
        const anterior = document.querySelector('#encabezado');
        //elemento padre
        const elPadre = document.querySelector('#lista-cursos');
        
        //Remplazar
        elPadre.replaceChild( nuevoEncabezado, anterior);
        console.log(nuevoEncabezado);*/
    

    //Eliminar elementos ( nodos )

       /* const enlaces = document.querySelectorAll('.enlace');
        const navegacion = document.querySelector('#principal');
        
        //enlaces[0].remove();
        //navegacion.removeChild(enlaces[0]);

        console.log(enlaces);*/

//EventListener

    //EventListener click al buscador

       /* document.querySelector('#submit-buscador').addEventListener('click',function (e) {
            e.preventDefault();
            alert(' Buscando cursos espere un momento');
        })
        */
    //EventListener Variables
       /* const encabezado = document.querySelector('#encabezado');
        const enlaces = document.querySelectorAll('.enlace');
        const boton = document.querySelector('#vaciar-carrito');
        const busqueda = document.querySelector('#buscador');
        const card = document.querySelector('.card');
        const infoCurso = document.querySelector('.info-card');
        const agregarCarrito = document.querySelector('.agregar-carrito');

        //keydown
            //busqueda.addEventListener ('keydown',obtenerEvento);
        //keyup
            //busqueda.addEventListener ('keyup',obtenerEvento);
        //keypress
            //busqueda.addEventListener ('keypress',obtenerEvento);


        //click
            //boton.addEventListener('click', obtenerEvento);
        //Doble Click
            //boton.addEventListener('dblclick', obtenerEvento);
        //Mouse enter
            //boton.addEventListener('mouseenter', obtenerEvento);
        //Mouse leave
            //boton.addEventListener('mouseleave', obtenerEvento);
        //mouse over
            //boton.addEventListener('mouseover', obtenerEvento);
        //mouse out
             //boton.addEventListener('mouseout', obtenerEvento);
        //mousedown
            //boton.addEventListener('mousedown', obtenerEvento);
        //mouseup
            //boton.addEventListener('mouseup', obtenerEvento);
        //mouse move
            //boton.addEventListener('mousemove', obtenerEvento);


        //Focus
           // busqueda.addEventListener ('focus',obtenerEvento);
        //BLUR
            //busqueda.addEventListener ('blur',obtenerEvento);
        //cut
            //busqueda.addEventListener ('cut',obtenerEvento);
        //copy
            //busqueda.addEventListener ('copy',obtenerEvento);            
        //paste
            //busqueda.addEventListener ('paste',obtenerEvento);
        //input se ejecuta cada que pasa cualquier evento de los de arriba
            //busqueda.addEventListener ('input',obtenerEvento);
        //change se ejecuta cuando detecta algun cambio
            //busqueda.addEventListener ('change',obtenerEvento);


        card.addEventListener('click',function (e) {
            console.log('Click en Card');
            e.stopPropagation();
        });
        
        infoCurso.addEventListener('click',function (e) {
            console.log('Click en infoCurso');
            e.stopPropagation();
        });

        agregarCarrito.addEventListener('click',function (e) {
            console.log('Click en agregarCarrito');
            e.stopPropagation();
        });



        function obtenerEvento(e) {
            //console.log(busqueda.value);
            console.log(`Evento: ${e.type}` );
        }
        */

//Delegation
   /* document.body.addEventListener('click',eliminarElemento);

    function eliminarElemento(e) {
        e.preventDefault();
        console.log('Click');
        if (e.target.classList.contains('borrar-curso')) {
            console.log('SI');
        }else{
            console.log('NO');
            
        }
    }*/

//LocalStorage
 /*   //Agregar a localStorage
        localStorage.setItem('nombre', 'Alex');
    // Session Storage
        sessionStorage.setItem('nombre', 'Alex')  
    //Obtener Valor de localStorage
        console.log(localStorage.getItem('nombre'));
        
    // Eliminar de localStorage
        localStorage.removeItem('nombre'); */


//Object
    /* //Metodo Objetos
    function Cliente( nombre, saldo) {
        this.nombre = nombre;
        this.saldo = saldo;
        
    }


        //Crear prototipos
        Cliente.prototype.tipoCliente = function (Edad) {
            let tipo;

            if (this.saldo > 1000) {
                tipo = 'Gold';
            } else if (this.saldo > 500) {
                tipo = 'Platinum';
            }else{
                tipo = 'Normal';
            }

            return tipo;
        }

        //Retiro
        Cliente.prototype.nombreClienteSaldo = function () {
            return `Nombre: ${this.nombre}, Tu saldo es de ${this.saldo}`
        }


        const persona = new Cliente('Pedro', 20000)

        console.log( persona );




    function Empresa(nombre, saldo, telefono, tipo) {
        Cliente.call(this, nombre, saldo);
        this.telefono = telefono;
        this.tipo = tipo;
    }

    Empresa.prototype = Object.create(Cliente.prototype);

    const empresa = new Empresa( 'Udemy',10000000, 12091398, 'Educacion');

    console.log(empresa.nombreClienteSaldo());
    */
    



// !!!!!!!!!! IMPORTANTE CLASES ¡¡¡¡¡¡¡¡¡¡¡

    // Clases
    /*    class Cliente{

            constructor(nombre, apellido, saldo){
                this.nombre = nombre;
                this.apellido = apellido;
                this.saldo = saldo;
            }

            tipoCliente = function () {
                let tipo;
    
                if (this.saldo > 1000) {
                    tipo = 'Gold';
                } else if (this.saldo > 500) {
                    tipo = 'Platinum';
                }else{
                    tipo = 'Normal';
                }
    
                return tipo;
            }

            retirarSaldo(retiro){
                return this.saldo -= retiro;
            }

        }


        const Mary = new Cliente( 'Mary', 'Perez', 100000);

        console.log(Mary);
        

    class Empresa extends Cliente{

        constructor( nombre,saldo, telefono, tipo){
            
            //accede al constructor padre
                super(nombre, saldo);
            //No existen en el constructor padre asi que nosotros le damos valor    
                this.telefono = telefono;
                this.tipo = tipo;
        }

    }
    
    
    const empresa = new Empresa( 'EmpresaX', 100000000, 651894651, 'empresa'); */