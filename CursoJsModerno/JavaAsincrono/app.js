

//const ciudades = ['Londres', 'New York', 'Madrid','Paris','Viena'];
/*
//inline callbacks (normalmente utilizado)
ciudades.forEach(function (ciudad) {
    console.log(ciudad);
})*/

/*
//con funcion definida (no muy utilizado)
function callback(ciudad) {
    console.log(ciudad);
}
ciudades.forEach(callback);*/

//Listado de paises
/*
const paises = ['Francia','España','Portugal','Australia','Inglaterra','Irlanda'];


function nuevoPais(pais, callback) {
    setTimeout(function () {
        paises.push(pais);
        callback();
    },2000);
}


function mostrarPaises() {
    setTimeout(function () {
        let html = '';
        paises.forEach(function (pais) {
            html += `<li>${pais}</li>`;
        });
        document.getElementById('app').innerHTML = html;
    },1000);
}


//Agregamos Pais
nuevoPais("Mexico",mostrarPaises);
//mostramos paises
mostrarPaises();
*/

//Promises
/*
const esperando = new Promise(function (resolve, reject) {
     setTimeout(function() {
         resolve('Se ejecuto');
     },5000);
});

esperando.then(function(mensaje) {
    console.log(mensaje);
    
});*/

const aplicarDescuento = new Promise(function (resolve, reject) {
    const descuento = false;
    if (descuento) {
        resolve('Descuento Aplicado');
    }else{
        reject('No se puede aplicar el descuento');
    }
});

aplicarDescuento.then(function(resultado){
    console.log(resultado);
    
}).catch(function (error) {
    console.log(error);
    
})

