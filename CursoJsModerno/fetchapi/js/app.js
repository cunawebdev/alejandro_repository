document.getElementById('txtBtn').addEventListener('click', cargarTXT);
document.getElementById('jsonBtn').addEventListener('click', cargarJSON);
document.getElementById('apiBTN').addEventListener('click', cargarAPI);    


/*
    //txt
    function cargarTXT() {
        fetch('datos.txt').then(function (res) {
            return res.text();
        })
        .then(function (data) {
            console.log(data);
            document.getElementById('resultado').innerHTML = data;
        })
        .catch(function (error) {
            console.log(error);
            
        })
    }

    //JSON
    function cargarJSON() {
        fetch('empleados.json')
        .then(function (res) {
            return res.json();
        })
        .then(function (data) {
            let html = '';
            data.forEach(function (empleado) {
                html += `
                    <li>${empleado.nombre} : ${empleado.puesto}</li>
                `;
            })
            document.getElementById('resultado').innerHTML = html;
        })
        .catch(function (error) {
            console.log(error);
            
        })
    }
    //API
    function cargarAPI() {
        fetch('https://picsum.photos/list')
        .then(function (res) {
            return res.json();
        })
        .then(function (imagenes) {
            let html = '';

            imagenes.forEach(function (imagen) {
                html += `
                    <li>
                        <a target= "_blank" href="${imagen.post_url}">Ver Imagen</a>
                        ${imagen.author}
                    </li>
                `;
            });
            document.getElementById('resultado').innerHTML = html;
        })
        .catch(function (error) {
            console.log(error);
            
        })
    }

*/

    //txt
        function cargarTXT() {
            fetch('datos.txt').then( res => res.text())
            .then(data => document.getElementById('resultado').innerHTML = data)
            .catch(error => console.log(erro));
        }
        //JSON
        function cargarJSON() {
            fetch('empleados.json')
            .then(res => res.json())
            .then(data => {
                let html = '';
                data.forEach(empleado => {
                    html += `
                        <li>${empleado.nombre} : ${empleado.puesto}</li>
                    `;
                })
                document.getElementById('resultado').innerHTML = html;
            })
            .catch(error => console.log(erro));
        }
        //API
        function cargarAPI() {
            fetch('https://picsum.photos/list')
            .then(res => res.json())
            .then(imagenes => {
                let html = '';
                imagenes.forEach(imagen => {
                    html += `
                        <li>
                            <a target= "_blank" href="${imagen.post_url}">Ver Imagen</a>
                            ${imagen.author}
                        </li>
                    `;
                });
                document.getElementById('resultado').innerHTML = html;
            })
            .catch(error => console.log(error));
        }