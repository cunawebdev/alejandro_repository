
/* Curso JS: Clase 5 
    var a = 1;
    var b = "Fernando";

    console.log( a );
    console.warn( a );
    console.error( a );
    console.info ( a );

    console.log( a + a ) 

*/

/* Curso JS: Clase 6 

    console.log( a );
    console.log ( b );

    var a = 1;
    var b = "Fernando";

    console.log( a );
    console.log ( b );

*/

/* Curso JS: Clase 7 - Enseña si JS es multihilo

    function imprimir(){

        for ( var i = 0; i<8000; i++ ){

            console.log("Imprimio");

        }
    }

    function presionoClick(){
        console.log("Presiono Boton")
    }

    imprimir();
*/

/* Curso JS: Clase 9 Tipos de datos 

    // Tipos Primitivos 
    var num = 10;
    var str = "texto";
    var bol = true;
    var und = undefined;
    var nul = null;

    // Objetos 

    var obj = {

        numero : 10, // separar atributos por " , "
        texto : "objTexto", // Ultimo atributo no lleva nada al final 

        // Se puede definir un obj dentro de otro sin limite 
        objHijo : {
            
            numeroHijo : 10,
            textoHijo : "textoHijo"

        }

    };

    console.log(obj);

*/

/* Curso JS: Clase 9 Por valor y Referencia 

    var a = 10;
    var b = a;

    console.log( "a :", a);
    console.log( "b :", b);

    var c = {

        nombre : "obj"

    }

    var d = c;

    console.log( "c :", c);
    console.log( "d :", d);

    c.nombre = "txt";

    console.log( "c :", c);
    console.log( "d :", d);

    Dado que los obj " c " y " d " estan igualados JS reserva memoria para crear el obj c y hace que el obj " d "
    apunte al mismo espacio de memoria asi estos siempre tienen los mismos valores esto es el "Valor por Referencia" */

/* Curso JS: Clase 11 Notacion Punto y Corchete 

    //Notacion de Punto

    var persona = {
        
        nombre : "Pepe",
        apellido:  " Hernandez",
        edad : 25 ,

        direccion : {
            pais : " España ",
            ciudad : " Madrid ",
            edificio : {
                nombre : "Pricipal",
                telefono : 123456789
            }
        }
    };

    console.log( persona.direccion);

    persona.direccion.zipcode = 82150;

    console.log( persona.direccion); 

    //Notacion de corchete

    var campo = "edad";

    console.log (  persona[campo] ); */

/* Curso JS: Clase 12 - 14 Funciones 

    var a = 2;

    function ejemFuncion(  ){

        //persona.apellido = persona.apellido || "xxx";
        //console.log( persona.nombre + " " + persona.apellido ); 
    
        return Math.random();
    }

    function getNombre(){
        return "Juan";
    }

    function crearPersona( nombre, apellido ){

        return {
            nombre: nombre,
            apellido: apellido
        }

    }

    function crearFuncion (){

        return function(nombre) {
            console.log("Me creo" +" "+ nombre); 

            return function() {
                console.log("Segunda Funcion");
            }
        }
    }

    var xPersona = crearPersona( "Pepe", "Pecas");


    //var miFuncion = ejemFuncion;
    //ejemFuncion( xPersona );
    //var a  = 1;
    var nuevaFuncion = crearFuncion();

    var segundaFuncion = nuevaFuncion( xPersona.nombre);
    segundaFuncion();*/

/*Curso JS: Clase 15 Funciones de Primera Clase

        Las funciones son objetos
        Las funciones de Primera Clase se les puede añadir propiedades o metodos,
        asi como sobreescribir por las funciones o propiedades de esta pero con puedes
        cambiar por Ejemplo el nombre de la funcion



    function a() {
    }
    //a.name = "APA";
    //console.log( a.name);

    var persona = {

    Clase 16 Metodos y obj THIS

        nombre: "Maria",
        apellido: "Dubon",
        imprimirNombre : function () {
            console.log( this.nombre + " " + this.apellido);
        },
        direccion:{
            pais: "Mexico",
            getPais: function () {

                var self = this; // Este self apunta al this antes de la funcion getPais ya que si se crea un ¿error?
                
                var nuevaDireccion = function () {
                    console.log( "La nueva direccion es en " + self.pais);
                }

                nuevaDireccion();

            }
        }
    }

    persona.imprimirNombre();
    persona.direccion.getPais();

*/

/*Curso JS: Clase 17 Palabra reservada new 

        new crea un Objeto Vacio pero es necesario poner el tipo del objeto 




    function Persona( nombre, apellido, edad) {

        this.nombre = nombre || this.nombre || "??";
        this.apellido = apellido || this.apellido || "??";
        this.edad = edad || this.edad || "??";

        this.imprimirPersona = function () {
            return this.nombre + " " + this.apellido + "(" + this.edad + ")";
        }
    }

    var juan = new Persona("Juan", "Pecas" , "12" );
    console.log(juan.imprimirPersona());*/


/* Curso JS: Clase 18 Señor de los Anillos Game 

    function Jugador(nombre) {
        
        this.nombre = nombre;
        this.pv = 100;
        this.sp = 100;

        this.curar = function( jugadorObjetivo ) {
            if(jugadorObjetivo.pv > 0){
                if(this.sp >= 40){
                    this.sp -= 40;
                    jugadorObjetivo.pv += 20;
                }else{

                    console.log(this.nombre + " necesita mas mana");

                }
            }else{
                console.error(jugadorObjetivo.nombre + " esta muerto");
                this.estado(jugadorObjetivo);
            }     
        }

        this.atacar = function (jugadorObjetivo) {
            if( jugadorObjetivo.pv > 20){
                jugadorObjetivo.pv -= 20;
            }else{
                jugadorObjetivo.pv = 0;
                console.error(jugadorObjetivo.nombre + " a muerto");
            }
                
            this.estado(jugadorObjetivo);
        }

        this.estado = function (jugadorObjetivo) {
            console.info (this);
            console.info( jugadorObjetivo );
        }

    }


    var gandalf = new Jugador("Gandalf");
    var legolas = new Jugador("Legolas");


    console.log(gandalf);
    console.log(legolas);
*/

/*Curso JS: Clase 19 Prototipo 
    function Persona( nombre, apellido, edad, pais) {

        this.nombre = nombre || this.nombre || "??";
        this.apellido = apellido || this.apellido || "??";
        this.edad = edad || this.edad || "??";
        this.pais = pais ||  this.pais || "??";

        
    }

    Persona.prototype.imprimirInfo = function () {
        console.log(this.nombre + " " + this.apellido + "(" + this.edad + ")," + this.pais);
    }

    var fer = new Persona( "Fernanda", "Herrera", "18", "Mexico");

    console.log( fer );
    fer.imprimirInfo(); */

//Curso JS: Clase 20 Funciones Anonimas 

    //var a = 1;
    /*(function () {
        
        var a = 2;
        
        console.log( a );
        console.log( this.a );

        a = function () {
            a = 3;
            return a;
        }();

        console.log(a);
        console.log(this.a);
    })*/

      /*function ejecutarFuncion( fn ) {
            if( fn() === 1 ){
                return true;
            }else{
                return false;
            }
            

            return true;    
        
        };

        console.log(
            ejecutarFuncion(function () {
                console.log("Funcion Anonima Ejecutada");
                return 0;
            })
        );    
*/

/* Curso JS: Clase 21 Typeof e InstanceOf 

function identificacion ( params ) {
    
    if ( typeof params == "function"){
        params();
    }else if( params instanceof Persona){
        console.log( " Es de tipo Persona ");
    }else{
        console.log(params);
    }

}

function  Persona() {
    
}

var juan = new Persona();

identificacion( juan ); */

/* Curso JS: Clase 22 Arreglos 1 

    var arr = [5,4,3,2,1];
    
    console.log( arr);

    // Invertir un arreglo REGRESA UN ARREGLO
    arr = arr.reverse();
    console.log( arr );

    //Recorre un arreglo y ejecuta una funcion en cada elemento del arreglo sin ciclo REGRESA UN ARREGLO

   /* arr = arr.map( function (elem) {
        elem *= 2;
        return elem;        
    })
    console.log( arr ); */

    /*Crea un string del arreglo

     arr = arr.join(" - ");
    console.log( ar );*/

    /*Corta el arreglo

    arr = arr.split("|");
    console.log( ar ); 

    // Mete un dato al final del arreglo

    arr.push(6);
    console.log( arr );

    // Mete un dato al pricipio del arreglo
    
    arr.unshift(0);
    console.log( arr );

    //toString
    console.log(arr.toString());

    //Elimina el ultimo elemento del arreglo

    var eliminada = arr.pop();
    console.log("Arreglo -->" + arr, "Variable eliminada "+ eliminada );

    //Elimina apartir de la posicion especificada (posicion, nElementosAeliminar,insert1,insert2)
    //despues de los elementos a eliminar todos los parametros los insertara
    console.log( arr );
    arr.splice( 1, 1, 10 );
    console.log( arr );

    //Corta un parte del arreglo ( primer corte, segundo corte o elementos dentro de lo cortado)
    console.log( arr );
    var a = arr.slice( 0 , 2);
    console.log( a );
    */

    /* Curso JS: Clase 23 Arreglos 2 

        var arr = [
            true,//0
            {
                nombre: "Pepe",
                apellido: "Pecas"//1
            },
            function () {
                console.log(" Dentro del arreglo ");//2
            },
            function( persona ) {

                console.log( persona.nombre+ " " + persona.apellido);//3

            },
            [ //4
                "Fernando",//0
                "Carlos",//1
                "Daniel",//2
                "Melisa",//3
                function () {//4
                    console.log( this );
                },//5
                [
                    "Juan",//0
                    "Cristobal",//1
                    "Rafa",//2
                    function () {//3
                        console.log( this );
                    }
                ]
            ],//5
            function () {
                console.log( this );
            }
        ];

        arr[3]( arr[1] );

        console.log( arr[4][5][3]() );*/

/* Curso JS: Clase 24 Arguments 

        var argument = 10;

        function miFuncion(a,b,c,d) {
            
            if ( arguments.length != 4 ){
                console.error( "La funcion necesita 4 parametros ");
                return; // para que salga directamente de la funcion
            }

            //console.log( arguments );

            console.log( a + b + c + d );

        }

        //los argumentos se refieren a la cantidad de parametros mandados a una funcion
        miFuncion(10, 20, 30, 40); */


//Clase 25 sobrecarga no hay pero se puede jugar con los parametros EJEMPLO CLASE 19 o jugar con los nombres de las funciones

//Calse 26 Polimorfismo recordar las funciones typeof y instanceof (CLASE 21) usando if o switch
        
/*Curso JS: Clase 27 Jugar con los contextos 

    function crearFunciones() {

        var arr = [];
        var numero = 1;

            for ( var numero = 1; numero <= 5; numero++ ){              

                arr.push(
                    (function ( numero ) {
                        return function () {
                        console.log( numero );   
                        }
                    })(numero)
                );
            }
        return arr;    
    }

    var funciones = crearFunciones();

    funciones[0]();
    funciones[1]();
    funciones[2]();
    funciones[3]();
    funciones[4](); */

/*Curso JS: Clase 30 STRING 

    Las cadenas no son ARREGLOS son OBJETOS

    toUpperCase -> Hace las letras mayusculas
    toLowerCase -> Hace las letras minusculas
    indexOF -> te da el primer indice del elemento que estes buscando
    lastIndexOf -> te da el ultimo indice del elemento que estes buscando
    substr -> corta a partir del indice que le des y el segundo parametro corta desde el primer indice dado hasta los nIndices indicados
        substr ( primerCorte, segundoCorte(num de posiciones despues del primer corte) )
    
    String puede usar la funcion split regresa un ARREGLO
*/

    
/*Curso JS: Clase 31 DATE 
    Moment.js -> libreria para fechas
    Las fechas son OBJETOS


    var hoy = new Date();
    var fMili = new Date(0);
    var fFija = new Date ( 2016, 9, 18, 22, 59, 59,999 );

    console.log( hoy );
    console.log( fMili );
    console.log( fFija );
     
    console.log( fFija.getFullYear() );


    //===================================================================


    var fecha = new Date( 2016, 9, 20 );

   // console.log( fecha );
   // fecha.setDate( fecha.getDate() + 5);
   //  console.log( fecha );

   Date.prototype.sumarDias = function( dias ) {

        this.setDate( this.getDate() + dias );
        return this;   

   }

   Date.prototype.sumarAnios = function( anios ) {

        this.setDate( this.getFullYear + anios );
        return this;

    }

   console.log( fecha );
   console.log( fecha.sumarDias(5) );

/*Curso JS: Clase 33 MATH 

    var PI = Math.PI;
    var E = Math.E;

    var num = 10.12353;
    
    console.log( num );
    console.log( Math.round( num*100) / 100); //multipicas por el numero de decimales y divides para que lo muestre correctamente

    //funcion floor quita todos los decimales de un numero sin redondear

    function randomEntre( min, max ) {
        return Math.floor( Math.random() * ( max-min + 1 ) + min ); 
    }

    console.log ( randomEntre( 1, 6 ) ); 
*/
    /* Expresiones Regulares
    
    ^ ---> busca al principio de la cadena
    $ ---> busca al final de la cadena
    . ---> Cualquier caracter
    \s ---> Cualquier separacion en el texto
    \w ---> [a-zA-Z0-9]
    \d ---> [0-9]
    
    escribir al final de la expresion
        i ---> insensible a mayus y minus
        g ---> todas la apariciones o ocurrencias
        m ---> multilinea

    Estrucutras de Repeticion
        o ---> estar seguro que aparece lo inidicado 1 o mas veces
        ? ---> Puede o no aparecer la expresion
        * ---> Combinacion de las 2 anteriores. Si aparece 0 o mas veces        

        mozilla developert expresion
    */

  //  var reg = /a/ ; // Puede incilizar tambien var reg = RegExp(expresion)

/*Curso JS: Clase 43 Funciones Especiales 

    var carro = [
           asd,
           asda,
           asd 
    ]

    bind() ---> ayuda a apuntar a cierto contexto en concreto
    funciones prestadas
        call()
        apply()
*/
    
/* Curso JS : Clase 50 Funciones de tiempo de JS 

    setTimeOut() da un retraso o ejecuta algo despues de cierto tiempo
    setInterval() ejecuta algo continuamente
    clearInterval() rompe el intervalo para dejar de ejecutar
*/

