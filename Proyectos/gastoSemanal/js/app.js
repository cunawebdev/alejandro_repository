// Variables

    const presupuestoUsuario = prompt('Cual es tu presupuesto Semanal?');
    const formulario = document.getElementById('agregar-gasto');
    let cantidadPresupuesto;


    //console.log(presupuestoUsuario);

//Clases
//Clase de Presupuesto
    class Presupuesto {
        constructor(presupuesto){
            this.presupuesto = Number(presupuesto);
            this.restante = Number(presupuesto);
        }
        //Metodo
        presupuestoRestante( cantidad = 0){
            return this.restante -= Number(cantidad);
        }

    }

//Clase Interfaz Maneja lo relacionado con el HTML
    class Interfaz {
        insertarPresupuesto(cantidad){
            const presupuestoSpan = document.querySelector('span#total');
            const restanteSpan = document.querySelector('span#restante');
            
            //Insertar en el html
                presupuestoSpan.innerHTML = `${cantidad}`;
                restanteSpan.innerHTML = `${cantidad}`;
        }

        imprimirMensaje(mensaje, tipo){
            const divMensaje = document.createElement('div');
            divMensaje.classList.add('text-center', 'alert');
            if (tipo ==='error') {
                divMensaje.classList.add('alert-danger');
            } else {
                divMensaje.classList.add('alert-success');
            }
            divMensaje.appendChild(document.createTextNode(mensaje));
            //Insertar en el DOM
            document.querySelector('.primario').insertBefore(divMensaje, formulario);

            //Quitar Alert
            setTimeout(function(){
                document.querySelector('.primario .alert').remove();
                formulario.reset();
            },3000);
        }

        agregarGastoListado( nombre, cantidad){
            const gastosListado = document.querySelector('#gastos ul');
            
            //Crear un LI
            const li = document.createElement('li');
            li.className = 'list-group-item d-flex justify-content-between align-items-center';
            
            //insertar al gasto
            li.innerHTML = ` ${nombre} 
            <span class="badge badge-primary badge-pill">$ ${cantidad} </span>`;

            //insertar al HTML
            gastosListado.appendChild(li);
        }

        //Comprueba el Presupuesto restante
        presupuestoRestante(cantidad){
            
            const restante = document.querySelector('span#restante');
           
            //Leer el presuepuesto restante
            const presupuestoRestanteUsuario = cantidadPresupuesto.presupuestoRestante(cantidad);
            restante.innerHTML = `${presupuestoRestanteUsuario}`;

            this.comprobarPresupuesto();
        }

        //Verificar el Presupuesto restante
        comprobarPresupuesto(){
            const presupuestoTotal = cantidadPresupuesto.presupuesto;
            const presupuestoRestante = cantidadPresupuesto.restante;

            //Comprobar el 25%
            if ( (presupuestoTotal / 4) > presupuestoRestante ) {
                const restante = document.querySelector('.restante');
                restante.classList.remove('alert-success', 'alert-warning');
                restante.classList.add('alert-danger');
            } else if ( (presupuestoTotal / 2) > presupuestoRestante){
                const restante = document.querySelector('.restante');
                restante.classList.remove('alert-success');
                restante.classList.add('alert-warning');
            }
        }
    }



//Event Listener
    document.addEventListener('DOMContentLoaded', function () {
        if (presupuestoUsuario === null || presupuestoUsuario === '') {
            window.location.reload();
        } else {
            
            //Instanciar el Presupuesto
            cantidadPresupuesto = new Presupuesto(presupuestoUsuario);

            
            //Intanciar clase interfaz
            const ui = new Interfaz();
            ui.insertarPresupuesto(cantidadPresupuesto.presupuesto);
        }
    });
    
    formulario.addEventListener('submit', function (e) {
        e.preventDefault();
        
        // Leer los gastos del Formulario
            const nombreGasto = document.querySelector('#gasto').value;
            const cantidadGasto = document.querySelector('#cantidad').value;

        //Instanciar la Interfaz
        const ui = new Interfaz();
        
        // Comprobar que los campos no esten vacios
        if (nombreGasto === '' || cantidadGasto === '') {
            //2 Parametos: Mensaje y tipo
            ui.imprimirMensaje('Hubo un error','error');
        } else {
            //Insertar en el HTML
            ui.imprimirMensaje('Agregado Correctamente', 'success');
            ui.agregarGastoListado(nombreGasto, cantidadGasto);
            ui.presupuestoRestante(cantidadGasto);
        }

    });